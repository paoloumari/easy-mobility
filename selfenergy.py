"""
Stuff for calcualting vibratuional part of self-energy
all units must be in HARTREE ATOMIC UNITS
"""
import numpy as np
import matplotlib.pyplot as plt
import vibrations
from distributions import der_fermi_distribution, bose_distribution
from math import pi, sqrt,log, exp
from conversions import ELECTRON_MASS_SI,HBAR_SI,KB_SI,HARTREE_SI,HARTREE_EV,ELEMENTARY_CHARGE_SI


class selfenergy:
    """ class for expectation values of self energy operator
    defined with respect to offset energy"""
    def __init__(self,eps,iband,isvalence,emass,ene_min,ene_max,npoints):
        self.omega=eps.omega
        self.iband=iband
        self.isvalence=isvalence
        self.imselfene=np.zeros(npoints)
        self.freq=np.zeros(npoints)
        self.emass=emass
        self.ene_min=ene_min
        self.ene_max=ene_max
        self.npoints=npoints
        self.eps=eps
        self.corrections=corrective_factors()
        self.corrections.read()
        if(self.isvalence):
            for i in range(npoints):
                freq=(ene_max-ene_min)/float(npoints)*float(i)+ene_min
                self.freq[i]=freq
            #where freq was also called 'offset'
                for j in range(len(eps.eposmax)):
                    if(eps.eposmax[j]>freq):
                        self.imselfene[i]=self.imselfene[i]-(1.0/(2*pi)**3)*\
                            (4.*pi)**2*self.emass*eps.inte_peak[j]/sqrt( \
                                2.*self.emass*(eps.eposmax[j]-self.freq[i]))
        else:
            for i in range(npoints):
                freq=(ene_max-ene_min)/float(npoints)*float(i)+ene_min
                self.freq[i]=freq
            #where freq was also called 'offset'
                for j in range(len(eps.eposmax)):
                    if(eps.eposmax[j]+self.freq[i]>0):
                        self.imselfene[i]=self.imselfene[i]-(1.0/(2*pi)**3)*\
                            (4.*pi)**2*self.emass*eps.inte_peak[j]/sqrt( \
                                2.*self.emass*(eps.eposmax[j]+self.freq[i]))


    

    def adjust_oscillators(self,temperature):
        self.temp=temperature
        if(self.isvalence):
            for i in range(self.npoints):
                self.imselfene[i]=0.
                freq=(self.ene_max-self.ene_min)/float(self.npoints)*float(i)+self.ene_min
                if(freq<0):
                    for j in range(len(self.eps.eposmax)):
                        if(self.eps.av_freq[j]<abs(freq)):
                            fact=bose_distribution(self.eps.av_freq[j]*HARTREE_SI,self.temp)+1.
                        else:
                            fact=bose_distribution(self.eps.av_freq[j]*HARTREE_SI,self.temp)

                        if(i==0):
                            #print('fact : '+str(fact))
                            kappa=sqrt(2*self.emass*(self.eps.eposmax[j]-self.freq[i]))
                            #print('kappa: '+str(kappa)+' '+str(1./kappa**2.))
                            

                        kappa=sqrt(2*self.emass*(self.eps.eposmax[j]-self.freq[i]))
                        kappap=sqrt(2*self.emass*(-self.freq[i]))
                        kappar=kappap/kappa
                       
                        c1=(1.- (kappar/self.corrections.delta-float(int(kappar/self.corrections.delta))))
                        c2= (kappar/self.corrections.delta-float(int(kappar/self.corrections.delta)))
                        fact_corr=self.corrections.cf[int(kappar/self.corrections.delta)]*c1+\
                                   self.corrections.cf[int(kappar/self.corrections.delta)+1]*c2

                        #fact_corr=self.corrections.cf[int(kappar/self.corrections.delta)]          
                        self.imselfene[i]=self.imselfene[i]-fact*fact_corr*(1.0/(2*pi)**3)*\
                                (4.*pi)**2*self.emass*self.eps.inte_peak[j]/sqrt( \
                                    2.*self.emass*(self.eps.eposmax[j]-self.freq[i]))
                        
        else:
           for i in range(self.npoints):
                self.imselfene[i]=0.
                freq=(self.ene_max-self.ene_min)/float(self.npoints)*float(i)+self.ene_min
                if(freq>=0):
                    for j in range(len(self.eps.eposmax)):
                        if(self.eps.av_freq[j]<abs(freq)):
                            fact=bose_distribution(self.eps.av_freq[j]*HARTREE_SI,self.temp)+1.
                        else:
                            fact=bose_distribution(self.eps.av_freq[j]*HARTREE_SI,self.temp)

                        kappa=sqrt(2*self.emass*(self.eps.eposmax[j]+self.freq[i]))
                        kappap=sqrt(2*self.emass*(self.freq[i]))
                        kappar=kappap/kappa
                       
                        c1=(1.- (kappar/self.corrections.delta-float(int(kappar/self.corrections.delta))))
                        c2= (kappar/self.corrections.delta-float(int(kappar/self.corrections.delta)))
                        fact_corr=self.corrections.cf[int(kappar/self.corrections.delta)]*c1+\
                                   self.corrections.cf[int(kappar/self.corrections.delta)+1]*c2
                        #fact_corr=self.corrections.cf[int(kappar/self.corrections.delta)]
                        self.imselfene[i]=self.imselfene[i]+fact*fact_corr*(1.0/(2*pi)**3)*\
                                (4.*pi)**2*self.emass*self.eps.inte_peak[j]/sqrt( \
                                    2.*self.emass*(self.eps.eposmax[j]+self.freq[i]))
                        
                            
                        


    def do_graph(self):
        x=np.ones(self.freq.shape)
        ise=np.ones(self.freq.shape)
        for i in range(self.freq.shape[0]):
            x[i]=self.freq[i]*(2.*13.6056980659*1000.)
            ise[i]=-self.imselfene[i]*(2.*13.6056980659*1000.)
       
        plt.plot(x,ise)
        """
        f = open('dati_schlimpf.dat',mode='r')
        line=f.readline()
        nn_cfr=int(line)
        x_cfr=np.zeros(nn_cfr)
        y_cfr=np.zeros(nn_cfr)
        for i in range(nn_cfr):
            line=f.readline()
            wordlist=line.split(',')
            x_cfr[i]=float( wordlist[0])
            y_cfr[i]=float( wordlist[1])
        plt.plot(x_cfr,y_cfr,'o')
        """
        plt.xlabel('meV')
        plt.ylabel('meV')
        plt.show()

    def print_graph(self):
        x=np.ones(self.freq.shape)
        ise=np.ones(self.freq.shape)
        for i in range(self.freq.shape[0]):
            x[i]=self.freq[i]*(2.*13.6056980659*1000.)
            ise[i]=-self.imselfene[i]*(2.*13.6056980659*1000.)
        
        f = open("self_energy_graph.dat",'w')
        for i in range(0,self.freq.shape[0]-1,10):
            f.write(str(x[i])+'  '+str(ise[i])+'\n')
        f.close()
       
      

    def do_graph_relaxation_time(self):
        
        x=np.ones(self.freq.shape)
        ise=np.ones(self.freq.shape)
        for i in range(self.freq.shape[0]):
            x[i]=self.freq[i]*(2.*13.6056980659*1000.)
            ise[i]=-HBAR_SI/(2*self.imselfene[i]*HARTREE_SI)
        plt.plot(x,ise)
        plt.xlabel('meV')
        plt.ylabel('s')
        plt.show()
        
class band_structure:
    """ class for the band structures  in AU
    offset relative to VBM and CBM , VBM set to 0 """
    def __init__(self,egap,el_mass,hl_mass,el_offset,hl_offset,eps,ene_min_se,ene_max_se,npoints_se):
        self.egap=egap
        self.el_mass=el_mass
        self.hl_mass=hl_mass
        self.el_offset=el_offset
        self.hl_offset=hl_offset
        self.n_el=len(self.el_mass)
        self.n_hl=len(self.hl_mass)

        
        self.se_hole=[]
        for i in range(self.n_hl):
            se=selfenergy(eps,i,True,self.hl_mass[i],ene_min_se,0.,npoints_se)
            self.se_hole.append(se)
        self.se_electron=[]   
        for i in range(self.n_el):
            se=selfenergy(eps,i,False,self.el_mass[i],0.,ene_max_se,npoints_se)
            self.se_electron.append(se)
            
            

    def set_temperature(self,temp,nk):
        self.temp=temp
        #calculate total electron density of states
        #units of SI
        
        for i in range(self.n_hl):
            self.se_hole[i].adjust_oscillators(self.temp)
        for i in range(self.n_el):       
            self.se_electron[i].adjust_oscillators(self.temp)
        
        
        self.nc=0.
        self.nc_check=0.
        for i in range(self.n_el):
            self.nc=self.nc+(sqrt(pi)/(8*pi**2.))*((2*self.el_mass[i]*ELECTRON_MASS_SI   \
                   /HBAR_SI**2.)**(3/2)) *(   KB_SI*self.temp)**(3/2)
            self.nc_check=self.nc_check+0.5*2.534*(self.el_mass[i]*self.temp/300.)**(3/2)

        self.nv=0.
        self.nv_check=0.
        for i in range(self.n_hl):
            self.nv=self.nv+(sqrt(pi)/(8*pi**2.))*((2*self.hl_mass[i]*ELECTRON_MASS_SI   \
                   /HBAR_SI**2.)**(3/2)) *(   KB_SI*self.temp)**(3/2)
            self.nv_check=self.nv_check+0.5*2.534*(self.hl_mass[i]*self.temp/300.)**(3/2)

        self.mu=0.5*self.egap+0.5*(1/HARTREE_SI)*KB_SI*self.temp*log(self.nv/self.nc)#mu is in AU

        self.n0=self.nc*exp(-(self.egap-self.mu)*HARTREE_SI/(KB_SI*self.temp))
        self.p0=self.nv*exp(-(self.mu)*HARTREE_SI/(KB_SI*self.temp))
                            

        #calculates hole mobility
        self.mob_hl_old=0.
        for iv in range(self.n_hl):
            de=abs(self.se_hole[iv].freq[1]-self.se_hole[iv].freq[0])*HARTREE_SI
            mobv=0.
            for j in range(self.se_hole[iv].freq.shape[0]):
                energy=self.se_hole[iv].freq[j]*HARTREE_SI
                if(energy<0):
                    rel_time=HBAR_SI/(2*self.se_hole[iv].imselfene[j]*HARTREE_SI)
                    #print('relaxation time : '+str(rel_time))
                    der=der_fermi_distribution(energy,self.mu*HARTREE_SI,self.temp)
                    #print('derivative : '+str(der))
                    hole_mass=self.hl_mass[iv]
                    pre=sqrt(2.*self.hl_mass[iv]*ELECTRON_MASS_SI)
                    pre=de*(-1.*energy)**(3./2.)*(8*pi)*pre
                    #print('prefactor . '+str(pre))
                    #mobv+=de*energy**(3./2.)*(8*pi)*sqrt(2.*self.hl_mass[iv]*ELECTRON_MASS_SI) \
                    #   *rel_time*der
                    mobv+=pre*rel_time*der

            self.mob_hl_old+=mobv
        self.mob_hl_old=self.mob_hl_old*ELEMENTARY_CHARGE_SI/self.n0/(HBAR_SI**3.)

        
        self.mob_hl=0.
        for iv in range(self.n_hl):
            ene_min_se=abs(self.se_hole[iv].freq[0])*HARTREE_SI
            de=abs(self.se_hole[iv].freq[1]-self.se_hole[iv].freq[0])*HARTREE_SI
            k_max=sqrt(2.*abs(ene_min_se)*self.hl_mass[iv]*ELECTRON_MASS_SI)/HBAR_SI
            mobv=0.
            for ik in range(nk):
                kx=k_max/float(nk)*float(ik)
                for jk in range(nk):
                    ky=k_max/float(nk)*float(jk)
                    for lk in range(nk):
                        kz=k_max/float(nk)*float(lk)
                        kappaq=kx**2.+ky**2.+kz**2.
                        kappa=sqrt(kappaq)
                        energy=HBAR_SI**2.*kappaq/(2.*self.hl_mass[iv]*ELECTRON_MASS_SI)
                        iene=int((-energy+ene_min_se)/de)
                        #print('Energy :'+' '+str(energy/ELEMENTARY_CHARGE_SI))
                        if(iene<0):
                            iene=0
                            #print('Ocio 0'+str(ik)+' '+str(jk)+' '+str(lk))
                        elif(iene>=self.se_hole[iv].imselfene.shape[0]):
                            iene=self.se_hole[iv].imselfene.shape[0]-1
                            #print('Ocio max')
                        
                        rel_time=HBAR_SI/(2.*self.se_hole[iv].imselfene[iene]*HARTREE_SI)
                        der=der_fermi_distribution(-energy,self.mu*HARTREE_SI,self.temp)
                        pre=(1./(2.*pi)**3.)*HBAR_SI**2.*kx**2./((self.hl_mass[iv]*ELECTRON_MASS_SI)**2.)
                        mobv+=pre*der*rel_time
                #print('relaxation time : '+str(rel_time))
                #print('derivative : '+str(der))
                #print('prefactor : '+str(pre))
            self.mob_hl+=8*mobv/float(nk)**3.*k_max**3.
        self.mob_hl=self.mob_hl*ELEMENTARY_CHARGE_SI/self.p0


        self.mob_el=0.
        for ic in range(self.n_el):
            ene_min_se=abs(self.se_electron[ic].freq[0])*HARTREE_SI
            ene_max_se=abs(self.se_electron[ic].freq[-1])*HARTREE_SI
            de=abs(self.se_electron[ic].freq[1]-self.se_electron[ic].freq[0])*HARTREE_SI
            k_max=sqrt(2.*abs(ene_max_se)*self.el_mass[ic]*ELECTRON_MASS_SI)/HBAR_SI
            mobc=0.
            for ik in range(nk):
                kx=k_max/float(nk)*float(ik)
                for jk in range(nk):
                    ky=k_max/float(nk)*float(jk)
                    for lk in range(nk):
                        kz=k_max/float(nk)*float(lk)
                        kappaq=kx**2.+ky**2.+kz**2.
                        kappa=sqrt(kappaq)
                        energy=HBAR_SI**2.*kappaq/(2.*self.el_mass[ic]*ELECTRON_MASS_SI)
                        iene=int((energy-ene_min_se)/de)
                        #print('Energy :'+' '+str(energy/ELEMENTARY_CHARGE_SI))
                        if(iene<0):
                            iene=0
                            #print('Ocio 0'+str(ik)+' '+str(jk)+' '+str(lk))
                        elif(iene>=self.se_electron[ic].imselfene.shape[0]):
                            iene=self.se_electron[ic].imselfene.shape[0]-1
                            #print('Ocio max')
                        
                        rel_time=HBAR_SI/(2.*self.se_electron[ic].imselfene[iene]*HARTREE_SI)
                        der=der_fermi_distribution(energy,-(self.egap-self.mu)*HARTREE_SI,self.temp)
                        pre=(1./(2.*pi)**3.)*HBAR_SI**2.*kx**2./((self.el_mass[ic]*ELECTRON_MASS_SI)**2.)
                        mobc+=pre*der*rel_time
                #print('relaxation time : '+str(rel_time))
                #print('derivative : '+str(der))
                #print('prefactor : '+str(pre))
            self.mob_el+=8*mobc/float(nk)**3.*k_max**3.
        self.mob_el=-self.mob_el*ELEMENTARY_CHARGE_SI/self.n0
            
            
       
                

        
      


    def print_values(self):
        print('EFFECTIVE ELCTRON DENSITY OF STATES (m-3) '+str(self.nc))
        print('EFFECTIVE ELCTRON DENSITY OF STATES CHECK( *10-19 cm-3) '+str(self.nc_check))
        print('EFFECTIVE HOLE DENSITY OF STATES (m-3) '+str(self.nv))
        print('EFFECTIVE HOLE DENSITY OF STATES CHECK( *10-19 cm-3) '+str(self.nv_check))
        print('CHARGE CARRIER  DENSITY OF STATES (m-3) '+str(self.n0))
        print('FERMI ENERGY  (eV):'+str(self.mu*HARTREE_EV))
        print('HOLE MOBILITY cm^2/(V*s) : '+str(self.mob_hl*100.**2))
        print('ELECTRON MOBILITY cm^2/(V*s) : '+str(self.mob_el*100.**2))



class corrective_factors:
    def __init__(self):
        self.delta=0.01

    def read(self):
        self.cf=[]
        self.kf=[]
        f=open('../corrective_factors.dat', mode='r')
        j=0
        for line in f:
            wordlist=line.split(',')          
            for i in range(len(wordlist)):
                  try:
                      ff=float(wordlist[i])
                      self.cf.append(ff)
                      self.kf.append(0.+self.delta*float(j))
                      j+=1
                  except ValueError:
                      pass
        f.close()
        
    
