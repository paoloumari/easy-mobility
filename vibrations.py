"""
Stuff for calcualting vibratuional spectra from pw.x
all units converted to HARTREE ATOMIC UNITS
"""
import numpy as np
import matplotlib.pyplot as plt
from distributions import bose_distribution
from conversions import HARTREE_SI,HARTREE_EV
from math import pi

class vibrations:
    """class for vibrational  data from phonon"""
    def __init__(self):
        self.omega=0.
        self.natoms=0
        self.skip=3
        self.epsilon_infy=1.0
        
    def readvibra(self,namepw,namedyn):
        """namepw is  the name of a pw.x output file
        and namedyn is the name of a dynamt autput file for the sam system"""   
        f = open(namepw,mode='r')


        self.atoms= []
        self.types= []
        self.masses=[]
        self.atoms_mass=[]
        read_atoms=False
        read_types=False
    
        while True:
            line = f.readline()
            if not line :
                break;
            wordlist=line.split()


           
            for i in range(len(wordlist)):
                if(wordlist[i]=='volume'):
                    self.omega=float(wordlist[i+2])
                    
                if(wordlist[i]=='types'):
                    self.ntypes=int(wordlist[i+2])

                if(wordlist[i]=='atoms/cell'):
                    self.natoms=int(wordlist[i+2])

                if(wordlist[i]=='site'):
                    read_atoms=True
                        
                if(wordlist[i]=='species' and wordlist[i+1]=='valence'):
                    read_types=True
            
            if(read_types):                                
                for it in range(self.ntypes):
                    line = f.readline()
                    wordlist=line.split()
                    self.types.append(wordlist[0])
                    self.masses.append(float(wordlist[2]))
                read_types=False
                
            if(read_atoms):
                for ia in range(self.natoms):
                    line = f.readline()
                    wordlist=line.split()
                    self.atoms.append(wordlist[1])
                read_atoms=False
                    
           
               
        f.close()
        for i in range(len(self.atoms)):
            for j in range(len(self.types)):
                if(self.atoms[i]== self.types[j]):
                    self.atoms_mass.append(self.masses[j])
                    break
        for i in range(len(self.types)):
            print('Type  : '+ str(i+1)+ ' '+self.types[i]+ ' '+str(self.masses[i])+'\n')
        for i in range(len(self.atoms)):
            print('Atom  : '+ str(i+1)+ ' '+self.atoms[i]+ '  '+str(self.atoms_mass[i])+'\n')
        
        f = open(namedyn,mode='r')
        found=False
        nmode=0
        self.freq= []
        self.inte= []
        for line in f:
            wordlist=line.split()
            if(found and nmode<self.natoms*3):
                self.freq.append(float(wordlist[1]))
                self.inte.append(float(wordlist[3]))
                nmode += 1
            else:
                for i in range(len(wordlist)):
                   if(wordlist[i]=='mode' and wordlist[i-1]=='#'):
                       found=True

        """convert to H AU"""
        for i in range(len(self.freq)):  
            self.freq[i] = self.freq[i]/(8065.6*2.0*13.6058)
        for i in range(len(self.inte)):
            self.inte[i] = self.inte[i]/10508.9*3.1415926/self.omega/3.
        f.close()

    def readmodes(self,namedynmat):
        """namepw is  the name of a pw.x output file
        and namedyn is the name of a dynamt autput file for the sam system"""   
        f = open(namedynmat,mode='r')
        self.vmode=np.zeros((3,self.natoms,self.natoms*3),'float64')
        iw=0
        jw=self.natoms+1
        for line in f:
            wordlist=line.split()
            if(len(wordlist)>0):
                if('omega' in wordlist[0]):
                        jw=0
                        continue
                else:
                    if(jw<self.natoms):
                        self.vmode[0,jw,iw]=float(wordlist[1])*self.atoms_mass[jw]**0.5
                        self.vmode[1,jw,iw]=float(wordlist[1+2])*self.atoms_mass[jw]**0.5
                        self.vmode[2,jw,iw]=float(wordlist[1+4])*self.atoms_mass[jw]**0.5
                        jw+=1
                        if(jw==self.natoms):
                            iw+=1
                            jw=self.natoms+1
                   
        f.close()
        for iw in range(self.natoms*3):
             sca=0.
             for kw in range(self.natoms):
                sca+=self.vmode[0,kw,iw]*self.vmode[0,kw,iw]+self.vmode[1,kw,iw]*self.vmode[1,kw,iw]+\
                self.vmode[2,kw,iw]*self.vmode[2,kw,iw]
             sca=1./sca**0.5
             for kw in range(self.natoms):
                for i in range(3):
                     self.vmode[i,kw,iw]=self.vmode[i,kw,iw]*sca                                                
                                                                      
    def readzetas(self,nameph):
        f = open(nameph,mode='r')
        self.zetas=np.zeros((3,3,self.natoms),'float64')
        while True:
            line =f.readline()
            if not line :
                break;
            wordlist=line.split()

            if(len(wordlist)>0):
                if(wordlist[0]=='Effective'):
                    line =f.readline()                
                    for ia in range(self.natoms):
                        line =f.readline()
                        for idir in range(3):
                            line =f.readline()
                            wordlist=line.split()
                            self.zetas[idir,0,ia]=float(wordlist[2])
                            self.zetas[idir,1,ia]=float(wordlist[3])
                            self.zetas[idir,2,ia]=float(wordlist[4])
        f.close()
                        
                        
            
        
        #check orthonormality
        """for iw in range(self.natoms*3):
            for jw in range(self.natoms*3):
                sca=0.
                for kw in range(self.natoms):
                    sca+=self.vmode[0,kw,iw]*self.vmode[0,kw,jw]+self.vmode[1,kw,iw]*self.vmode[1,kw,jw]+\
                          self.vmode[2,kw,iw]*self.vmode[2,kw,jw]
                print('ORTHONORMALITY : '+str(iw)+ '  '+str(jw)+ ' '+str(sca)+ '\n')"""
                
    def do_infra_stregths(self):
        from math import sqrt,pi,exp
        self.inte_calc=[]
        for iw in range(self.natoms*3):
            eosc=np.zeros(3,'float64')
            for ia in range(self.natoms):
                for idir in range(3):
                    for jdir in range(3):
                        eosc[jdir]+=self.vmode[idir,ia,iw]*self.zetas[jdir,idir,ia]/sqrt(self.atoms_mass[ia])
            self.inte_calc.append((eosc[0]**2.+eosc[1]**2.+eosc[2]**2.)/10508.9*3.1415926/self.omega/3.)
            if(self.inte[iw]>0):
                ratio=self.inte_calc[iw]/self.inte[iw]
            else:
                ratio=-1.
        
            print('RATIO OSC STRNGTH: ' +str(iw)+' '+str(ratio)+'\n')
            
                
            
        
    


    
        
def  gaussian(x,x0,sigma):
     from math import sqrt,pi,exp
     """calculates a 1D (normalised) gaussian with center at x0 and sigma at point x"""
     result=(1./(sigma*sqrt(2.*pi)))*exp(-0.5*((x-x0)/sigma)**2.)
     return result

def  peak(freq,freq0,inte,eta):
    """calculates the contribution to the complex dielectric function at freq for
    transition    in freq0 of intensity inte with broadening eta"""
    eps=inte/(freq0**2.0-(freq+eta*1.j)**2)
    return eps

class epsilon:
    """for calculating the complex dielectric function"""
    def __init__(self,vs,ene_min,ene_max,npoints,eps_infty,el_gap,eta,eta_el,nskip,freq_min=0.,temperature=0.):
        from math import sqrt
        self.omega=vs.omega
        self.freq=np.ones(npoints,'float64')
        self.eps=np.zeros(npoints,'complex64')
        self.eneloss=np.zeros(npoints,'float64')
        self.freq_min=freq_min
        self.temperature=temperature
        self.vs=vs
        self.eps_infty=eps_infty
        self.el_gap=el_gap
        self.ene_max=ene_max
        self.ene_min=ene_min
        self.npoints=npoints
        self.eta=eta
        self.eta_el=eta_el
        self.nskip=nskip
        self.n_freq_used=1
        for i in range(npoints):
            self.freq[i]=((ene_max-ene_min)/float(npoints)*float(i))
        w_p_e=sqrt((eps_infty-1.0)*el_gap**2.0)
        for i in range(npoints):
            for j in range(nskip,vs.natoms*3):
                self.eps[i]=self.eps[i]+peak(self.freq[i],vs.freq[j],vs.inte[j],eta)
            self.eps[i]=self.eps[i]+peak(self.freq[i],el_gap,w_p_e**2.,eta_el)
            self.eneloss[i]=(-1./self.eps[i]).imag

        etop=self.eneloss.max()
        einte=self.eneloss.sum()*(ene_max-ene_min)/float(npoints)
        self.eposmax=[]
        emax=[]
        e0=0.
        e1=1.
        for i in range(npoints):
            if(i>=1 and (i<npoints-2)):
                if(self.eneloss[i] > self.eneloss[i-1] and self.eneloss[i] > self.eneloss[i+1]):
                    if(self.eneloss[i] > etop/100.):
                        emax.append(self.eneloss[i])
                        self.eposmax.append(self.freq[i])
                        
        print('massimi:')
        print(self.eposmax)
        self.inte_peak=[]
        peaksum=0.
        for i in range(len(self.eposmax)):
            peaksum =  peaksum + emax[i]
        for i in range(len(self.eposmax)):
            self.inte_peak.append(einte*emax[i]/peaksum)

        self.av_freq=[]
        for i in range(len(self.eposmax)):
            omegap2=self.eposmax[i]*2.*self.inte_peak[i]/pi
            omega_av=sqrt(-omegap2+self.eposmax[i]**2.)
            self.av_freq.append(omega_av)
            
        

        print('intensità:')
        print(self.inte_peak)

        print('Oscillatori:')
        print(self.av_freq)

        self.enelossfit=np.zeros(npoints,'float64')
        for i in range(npoints):
            for j in range(len(self.eposmax)):
                self.enelossfit[i]=self.enelossfit[i]+gaussian(self.freq[i],self.eposmax[j],eta)*self.inte_peak[j]
                
            
        
        """for j in range(nskip,vs.natoms*3):
            w_plasmon=(vs.freq[j]**2.+vs.inte[j])**0.5
            for i in range(npoints):
                self.eneloss[i]+gaussian(self.freq[i],w_plasmon,eta)*vs.inte[j]/w_plasmon"""

        
        """add electronic peak"""
        """w_p_e=sqrt((eps_inf-1.d0)*w_av_e**2.d0)"""
          
        """w_p_e=w_p_e/(2.d0*13.6058)"""

    def adjust(self,freq_min,temperature):
        from math import sqrt
        self.eps=np.zeros(self.npoints,'complex64')
        self.eneloss=np.zeros(self.npoints,'float64')
        self.freq_min=freq_min
        if(self.temperature==temperature):
            n_freq=0
            for j in range(self.nskip,self.vs.natoms*3):
                if(self.vs.freq[j]<freq_min):
                    n_freq+=1
            if(n_freq==self.n_freq_used):
                return
            else:
                print('setting frequencies at :'+str(freq_min*HARTREE_EV))
        else:
            self.temperature=temperature
            
       
        w_p_e=sqrt((self.eps_infty-1.0)*self.el_gap**2.0)
        self.n_freq_used=0
        for j in range(self.nskip,self.vs.natoms*3):
                if(self.vs.freq[j]<freq_min):
                    self.n_freq_used+=1
        for i in range(self.npoints):
            for j in range(self.nskip,self.vs.natoms*3):
                if(self.vs.freq[j]<freq_min):
                    fact=1.+bose_distribution(self.vs.freq[j]*HARTREE_SI,temperature)
                else:
                    fact=bose_distribution(self.vs.freq[j]*HARTREE_SI,temperature)
                self.eps[i]=self.eps[i]+fact*peak(self.freq[i],self.vs.freq[j],self.vs.inte[j],self.eta)
            self.eps[i]=self.eps[i]+peak(self.freq[i],self.el_gap,w_p_e**2.,self.eta_el)
            self.eneloss[i]=(-1./self.eps[i]).imag

        etop=self.eneloss.max()
        einte=self.eneloss.sum()*(self.ene_max-self.ene_min)/float(self.npoints)
        self.eposmax=[]
        emax=[]
        e0=0.
        e1=1.
        for i in range(self.npoints):
            if(i>=1 and (i<self.npoints-2)):
                if(self.eneloss[i] > self.eneloss[i-1] and self.eneloss[i] > self.eneloss[i+1]):
                    if(self.eneloss[i] > etop/100.):
                        emax.append(self.eneloss[i])
                        self.eposmax.append(self.freq[i])
                        
        print('massimi:')
        print(self.eposmax)
        self.inte_peak=[]
        peaksum=0.
        for i in range(len(self.eposmax)):
            peaksum =  peaksum + emax[i]
        for i in range(len(self.eposmax)):
            self.inte_peak.append(einte*emax[i]/peaksum)

        print('intensità:')
        print(self.inte_peak)
        
        self.enelossfit=np.zeros(self.npoints,'float64')
        for i in range(self.npoints):
            for j in range(len(self.eposmax)):
                self.enelossfit[i]=self.enelossfit[i]+gaussian(self.freq[i],self.eposmax[j],self.eta)*self.inte_peak[j]
                
            
        
        

    def do_graph(self):
        a=self.freq.ndim
        print(a)
        x=np.ones(self.freq.shape)
        er=np.ones(self.freq.shape)
        eim=np.ones(self.freq.shape)
        el=np.ones(self.freq.shape)
        el2=np.ones(self.freq.shape)
        for i in range(self.freq.shape[0]):
            x[i]=self.freq[i]*(2.*13.6056980659)
            er[i]=self.eps[i].real
            eim[i]=self.eps[i].imag
            el[i]=(-1./self.eps[i]).imag
            el2[i]=self.enelossfit[i]
        plt.plot(x,er)
        plt.plot(x,eim)
        plt.plot(x,el)
        plt.plot(x,el2)
        plt.show()

            
        
        
                
