"""
Stuff for statistical distributions everything is in SI
"""

from math import exp

from conversions import KB_SI

def fermi_distribution(ene,mu,temp):
    f=1./(exp((ene-mu)/(KB_SI*temp))+1.)
    return f

def der_fermi_distribution(ene,mu,temp):
    f=-(exp((ene-mu)/(KB_SI*temp))+1.)**(-2.0)*exp((ene-mu)/(KB_SI*temp))/(KB_SI*temp)
    #print(str(f))
    return f
    
def bose_distribution(ene,temp):
    if(temp>10.):
        f=1./(exp((ene)/(KB_SI*temp))-1.)
    else:
        f=0.
    return f
