import vibrations
import selfenergy
from conversions import eV_to_Hau

def main():

    
    namescf = input('pw.x output file: \n')
    namedyn = input('dynmat.x output file: \n')
    """
    nameph = input('Phonon output file: \n')
    namedynmat = input('dynmat.x output  matrix file: \n')
    """
    
    if(namescf==''):
        namescf='PbI.ORTO.VC.vib.scf.out'
        print('    Using default:  '+namescf)
    if(namedyn==''):
        namedyn='PbI.ORTO.VC.vib.GIUSTINO.mod.out'
        print('    Using default:  '+namedyn)
    """
    if(namedynmat==''):
        namedynmat='PbI.ORTO.VC.vib.FREL.dynmat.out'
        print('    Using default:  '+namedynmat)
    if(nameph==''):
        nameph='ROT-DYN.25-200.vc.phG.out'
        print('    Using default:  '+nameph)
    """
    vs = vibrations.vibrations()
    vs.readvibra(namescf,namedyn)
    """
    vs.readmodes(namedynmat)
    vs.readzetas(nameph)
    vs.do_infra_stregths()
    """
    
    print('Volume a.u.     '+str(vs.omega))
    print('Number of atoms '+str(vs.natoms))
    print('Frequencies '+str(vs.freq))
    print('Intensities '+str(vs.inte))
    
    npoints  =  input('Number of steps: \n')
    if(npoints==''):
        npoints='100000'
        print('    Using default:  '+npoints)
    npoints=int(npoints)
    ene_min = input('E min (eV): \n')
    if(ene_min==''):
        ene_min='0.'
        print('    Using default:  '+ene_min)
    ene_min=float(ene_min)
    
    ene_max = input('E max (eV): \n')
    if(ene_max==''):
        ene_max='0.4'
        print('    Using default:  '+ene_max)
    ene_max=float(ene_max)
    eta= input('eta for phonons (eV): \n')
    if(eta==''):
        eta='0.00001'
        print('    Using default:  '+eta)
    eta=float(eta)
    el_gap = input('electronic gap (eV): \n')
    if(el_gap==''):
        el_gap='1.38'
        print('    Using default:  '+el_gap)
    el_gap=float(el_gap)
    eps_infty = input('High frequency dielectric constant: \n')
    if(eps_infty==''):
        eps_infty='5.9'
        print('    Using default:  '+eps_infty)
    eps_infty=float(eps_infty)
    eta_el= input('eta for electrons (eV): \n')
    if(eta_el==''):
        eta_el='0.001'
        print('    Using default:  '+eta_el)
    eta_el=float(eta_el)

    iband= input('Number of band for self_energy \n')
    if(iband==''):
        iband='18'
        print('    Using default:  '+iband)
    iband=int(iband)

    isvalence= input('Is it in valnece True/False: \n')
    if(isvalence==''):
        isvalence='True'
        print('    Using default:  '+isvalence)
    isvalence=bool(isvalence=='True')
    
    emass= input('emass for band (a.u.): \n')
    if(emass==''):
        emass='0.138'
        print('    Using default:  '+emass)
    emass=float(emass)

    npoints_se  =  input('Number of steps for self_energy : \n')
    if(npoints_se==''):
        npoints_se='10000'
        print('    Using def(ault:  '+npoints_se)
    npoints_se=int(npoints_se)
    
    ene_min_se = input('E min for self_energy or offset min (eV): \n')
    if(ene_min_se==''):
        ene_min_se='-0.8'
        print('    Using default:  '+ene_min_se)
    ene_min_se=float(ene_min_se)
    
    ene_max_se = input('E max for self_energy or offset max (eV): \n')
    if(ene_max_se==''):
        ene_max_se='0.8'
        print('    Using default:  '+ene_max_se)
    ene_max_se=float(ene_max_se)


    
    ene_min = eV_to_Hau(float(ene_min))
    ene_max = eV_to_Hau(float(ene_max))
    el_gap = eV_to_Hau(float(el_gap))
    eta = eV_to_Hau(float(eta))
    eta_el = eV_to_Hau(float(eta_el))
    ene_min_se = eV_to_Hau(float(ene_min_se))
    ene_max_se = eV_to_Hau(float(ene_max_se))

    eps=vibrations.epsilon(vs,ene_min,ene_max,npoints,eps_infty,el_gap,eta,eta_el,3)
    eps.do_graph()
    if(isvalence==True):
        se=selfenergy.selfenergy(eps,iband,isvalence,emass,ene_min_se,0.,npoints_se)
    else:
        se=selfenergy.selfenergy(eps,iband,isvalence,emass,0.,ene_max_se,npoints_se)
    se.do_graph()
    se.adjust_oscillators(1.)
    se.do_graph()
    se.print_graph()  
    se.do_graph_relaxation_time()
    se.adjust_oscillators(300.)
    se.do_graph()
    se.do_graph_relaxation_time()
    

    n_el = input('Number of electron bands: \n')
    if(n_el==''):
        n_el='2'
        print('    Using default:  '+n_el)
    n_el=int(n_el)
    
    n_hl = input('Number of hole bands: \n')
    if(n_hl==''):
        n_hl='2'
        print('    Using default:  '+n_hl)
    n_hl=int(n_hl)

    el_mass=[]
    hl_mass=[]
    el_offset=[]
    hl_offset=[]
    
    for i in range(n_el):
        el_mass.append(input('Electron mass band: '+ str(i)+ '..\n'))
        if(el_mass[i]==''):
            el_mass[i]='0.211'
        print('    Using default:  '+el_mass[i])
        el_mass[i]=float(el_mass[i])

    for i in range(n_hl):
        hl_mass.append(input('Hole mass band: '+ str(i)+ '..\n'))
        if(hl_mass[i]==''):
            hl_mass[i]='0.182'
        print('    Using default:  '+hl_mass[i])
        hl_mass[i]=float(hl_mass[i])

    for i in range(n_el):
        el_offset.append(input('Electron  band offset (eV): '+ str(i)+ '..\n'))
        if(el_offset[i]==''):
            el_offset[i]='0.0'
        print('    Using default:  '+el_offset[i])
        el_offset[i]=eV_to_Hau(float(el_offset[i]))
   

    for i in range(n_hl):
        hl_offset.append(input('Hole  band offset (eV): '+ str(i)+ '..\n'))
        if(hl_offset[i]==''):
            hl_offset[i]='0.0'
        print('    Using default:  '+hl_offset[i])
        hl_offset[i]=eV_to_Hau(float(hl_offset[i]))

    nk = input('Grid for k space integration: \n')
    if(nk==''):
        nk='400'
        print('    Using default:  '+nk)
    nk=int(nk)


    bd=selfenergy.band_structure(el_gap,el_mass,hl_mass,el_offset,hl_offset,eps,ene_min_se,ene_max_se,npoints_se)

    temperature = input('Temperature (K): \n')
    if(temperature==''):
        temperature='300'
        print('    Using default:  '+temperature)
    temperature=float(temperature)

    bd.set_temperature(temperature,nk)
    bd.print_values()

    mob_tot_h=bd.mob_hl
    mob_tot_e=bd.mob_el
    n_eli = input('Number of vibrations to be eliminated \n')
    if(n_eli!=''):
        n_eli=int(n_eli)
        n_int=(vs.natoms*3 - 3)//n_eli
        if(n_int*n_eli < vs.natoms*3):
            n_int+=1
        mobility_h=[]
        mobility_el=[]
        mobility_freq=[]
        for i in range( n_int):
            vs = vibrations.vibrations()
            vs.readvibra(namescf,namedyn)
            for j in range(n_eli):
                k=i*n_eli+j+2
                if(k<vs.natoms*3 ):
                    
                    vs.inte[k]=0.
            eps=vibrations.epsilon(vs,ene_min,ene_max,npoints,eps_infty,el_gap,eta,eta_el,3)
            bd=selfenergy.band_structure(el_gap,el_mass,hl_mass,el_offset,hl_offset,eps,ene_min_se,ene_max_se,npoints_se)
            bd.set_temperature(temperature,nk)
            mobility_freq.append(vs.freq[i*n_eli+2])
            mobility_el.append(bd.mob_el)
            mobility_h.append(bd.mob_hl)
            print('Electron mobility : ' + str(vs.freq[i*n_eli+2])+ ' '+str(bd.mob_el)+ ' \n')
            print('Hole mobility : ' + str(vs.freq[i*n_eli+2])+ ' '+str(bd.mob_hl)+ ' \n')
        print('Analysis Electrons \n')
        for i in range(len(mobility_freq)):
            print( str(mobility_freq[i])+'  '+str(mobility_el[i])+'  '+str(mob_tot_e))
        print('Analysis Holes \n')
        for i in range(len(mobility_freq)):
            print( str(mobility_freq[i])+'  '+str(mobility_h[i])+'  '+str(mob_tot_h))
            
      
    
if __name__ == "__main__":
    main()
    

